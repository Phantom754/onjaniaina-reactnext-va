const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')
const userRoute = require('./src/routes/user.routes')
const userGroupRoute = require('./src/routes/userGroup.routes')
const chatRoute = require('./src/routes/chat.routes')
const messageRoute = require('./src/routes/message.routes')
const app = express()
require('dotenv').config()
app.use(express.json())
app.use(cors({
    origin: "*"
}))
app.get('/',(req,res) => res.send('server chat'))
app.use('/api/users',userRoute)
app.use('/api/userGroup',userGroupRoute)
app.use('/api/chat',chatRoute)
app.use('/api/messages',messageRoute)
mongoose.connect(process.env.MONGO_CONNECTION, {useNewUrlParser: true, useUnifiedTopology:true}).then(() => {
    console.log('successfull connect');
    app.listen(process.env.PORT,() =>{
        console.log(`app server running at http://localhost:${process.env.PORT}`)
     })
}).catch(error => console.log(error?.message))