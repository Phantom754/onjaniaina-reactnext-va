const express = require('express')
const router = express.Router()
const UserController = require('../app/controller/user.controller')

router.post('/register',UserController.getAll)
router.post('/login',UserController.login)
router.get('/:id',UserController.findUser)
router.delete('/:id',UserController.delete)
router.get('/',UserController.findAllUser)

module.exports = router