const express = require('express')
const router = express.Router()
const UserGroupController = require('../app/controller/userGroup.controller')

router.post('/',UserGroupController.createUserToGroupe)
router.get('/',UserGroupController.findUserGroupe)

module.exports = router