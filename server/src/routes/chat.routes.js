const express = require('express')
const router = express.Router()
const ChatController = require('../app/controller/chat.controller')

router.post('/',ChatController.createChat)
router.get('/:userId',ChatController.findChat)
router.get('/find/:firstId/:secondId',ChatController.findUserChat)

module.exports = router