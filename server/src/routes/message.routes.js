const express = require('express')
const router = express.Router()
const MessageController = require('../app/controller/message.controller')

router.post('/',MessageController.createMessage)
router.get('/:chatId',MessageController.getMessages)
//router.get('/find/:firstId/:secondId',ChatController.findUserChat)

module.exports = router