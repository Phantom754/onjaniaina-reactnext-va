const chatModel = require('../models/chat.model')



class ChatController
 {
    static async createChat(req,res) {
      const {firstId,secondId} = req.body;
      try {
        const chat = await chatModel.findOne({
            members:{$all: [firstId,secondId]}
        })
        if(chat) {
            return res.json(chat)
        }
        const newChat = new chatModel({
            members:[firstId,secondId]
        })
        const response = await newChat.save()
        res.json(response)
      } catch (error) {
        
      }
    }
    static async findUserChat(req,res) {
        const {firstId,secondId} = req.params
        let userGroup = await chatModel.find({
            members:{$all: [firstId,secondId]}
        }).populate("members")
        res.json(userGroup)
    }

    static async findChat(req,res) {
        const {userId} = req.params
        let userGroup = await chatModel.find({
            members:{$in: [userId]}
        }).populate("members")
        res.json(userGroup)
    }

}

module.exports = ChatController