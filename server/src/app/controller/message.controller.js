const messageModel = require('../models/message.model')


class UserController {
   
  static async createMessage(req,res) {
    const {chatId,senderId,text} = req.body;
    const message = new messageModel({chatId,senderId,text})
    try {
        const response = await message.save()
        res.json(response)
    } catch (error) {
      
    }
  }
  static async getMessages(req,res) {
    const { chatId } = req.params;
    try {
       let message =await messageModel.find({chatId}).populate({path:"chatId",model:"Chat", populate: {
        path: 'members' 
    }}).populate("senderId")
       res.json({data:message})
    } catch (error) {
       res.json({error:error})
    }
  } 
  static async findAllUser(req,res) {
    try {
       let user =await messageModel.find()
       
       res.json({user:user})
    } catch (error) {
       res.json({error:error})
    }
  } 

  static async delete(req,res) {
    const {id} = req.params
    let user =await messageModel.findById(id)
    if(!user) {
        res.json({"message":"utilisateur introuvable"})
    }
    await user.deleteOne()
    res.json({"data": user,message:"suppression avec succéss"})
  }
}

module.exports = UserController