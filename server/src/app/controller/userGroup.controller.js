const userGroupModel = require('../models/userGroup.model')



class UserGroupController {
    static async createUserToGroupe(req,res) {
        let userGroup =new userGroupModel(req.body)
        await userGroup.save()
        res.json(userGroup)
    }
    static async findUserGroupe(req,res) {
        let userGroup = await userGroupModel.find().populate('users',"-password")
        userGroup = [...userGroup]
        if(Array.isArray(userGroup)) {
            userGroup= [...userGroup].map((group) =>({...group?._doc, nbUser: [...group.users].length}))
        }
        res.json(userGroup)
    }

    static async findUserGroupe(req,res) {
        let userGroup = await userGroupModel.findById(req.params.id).populate('users',"-password")
        userGroup = [...userGroup]
        if(Array.isArray(userGroup)) {
            userGroup= [...userGroup].map((group) =>({...group?._doc, nbUser: [...group.users].length}))
        }
        res.json(userGroup)
    }

    static async delete(req,res) {
        const {id} = req.params
        let userGroup =await userGroupModel.findById(id)
        if(!userGroup) {
            res.json({"message":"utilisateur groupe introuvable"})
        }
        await user.deleteOne()
        res.json({"data": userGroup,message:"suppression avec succéss"})
      }
}

module.exports = UserGroupController