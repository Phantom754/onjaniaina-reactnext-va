const userModel = require('../models/user.model')
const bcrypt = require('bcrypt')
const jwt = require("jsonwebtoken");
const validator = require("validator")

const createToken = (user) => {
    return jwt.sign({user},process.env.JWT_SECREY_KEY ,{expiresIn: '3d'})
 }
class UserController {
    static async getAll(req,res) {
        const {name,email,password} = req.body
        let user =await userModel.findOne({email})
        if(user) {
            return res.status(400).json({error:"Email exist"});
        }
        if(!name || !email || !password) {
            return res.status(400).json({error:"Toutes le champ est obligatoire"});
        }

        if(!validator.isEmail(email)){
            return res.status(400).json({error:"Email incorrect"});
        }
        user = new userModel({name,email,password})
        const salt = await bcrypt.genSalt(10)
        user.password = await bcrypt.hash(user.password,salt)
        await user.save()
        const token = createToken(user)
        res.json({user:user,token})
    }

   static async login(req,res) {
     const { email, password } = req.body;
     try {
        let user =await userModel.findOne({email})
        if (!user) {
            return res.status(400).json({error:"Invalide e-mail ou mot de passe"});
        }
        const isValidPassword = await bcrypt.compare(password,user.password)
        if(!isValidPassword) {
            return res.status(400).json({error:"Invalide mot de passe"});
        }
        const token = createToken(user)
        res.json({user:user,token})
     } catch (error) {
        res.json({error:error})
     }
   } 

  static async findUser(req,res) {
    const { id } = req.params;
    try {
       let user =await userModel.findById(id)
       
       res.json({user:user})
    } catch (error) {
       res.json({error:error})
    }
  } 
  static async findAllUser(req,res) {
    try {
       let user =await userModel.find().select('-password')
       
       res.json({user:user})
    } catch (error) {
       res.json({error:error})
    }
  } 

  static async delete(req,res) {
    const {id} = req.params
    let user =await userModel.findById(id)
    if(!user) {
        res.json({"message":"utilisateur introuvable"})
    }
    await user.deleteOne()
    res.json({"data": user,message:"suppression avec succéss"})
  }
}

module.exports = UserController