const mongoose = require('mongoose')
const chatSchema = mongoose.Schema({
    members:[{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
      }],//Array
},{ timestamps: { createdAt: 'created_at' } })

const chatModel = mongoose.model("Chat",chatSchema)
module.exports = chatModel 