const mongoose = require('mongoose')
const userSchema = mongoose.Schema({
   name: {
      type: String,
      required:true
   },
   email: {
    type: String,
    required:true,
    unique:true
   },
   password: {
    type: String,
    required:true
   },
   //groups: [{
     // type: mongoose.Schema.Types.ObjectId,
      //ref: 'UserGroup',
    //}],
},{ timestamps: { createdAt: 'created_at' } })

const userModel = mongoose.model("User",userSchema)
module.exports = userModel