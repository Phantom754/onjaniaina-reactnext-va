const mongoose = require('mongoose')
const userGroupeSchema = mongoose.Schema({
   name: {
      type: String,
      required:true
   },
   users: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    }],
},{ timestamps: { createdAt: 'created_at' } })

const userGroupModel = mongoose.model("UserGroup",userGroupeSchema)
module.exports = userGroupModel