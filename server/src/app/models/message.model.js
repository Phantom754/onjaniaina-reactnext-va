const mongoose = require('mongoose')
const messageSchema = mongoose.Schema({
   chatId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Chat',
  },
  senderId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  text: String
},{ timestamps: { createdAt: 'created_at' } })

const messageModel = mongoose.model("Message",messageSchema)
module.exports = messageModel 