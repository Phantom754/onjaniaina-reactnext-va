const { Server } = require("socket.io");

const io = new Server({ cors:"*" });
let onlineUsers = []

io.on("connection", (socket) => {
  //console.log("nouvelle connection",socket.id);
  socket.on('addNewUser', (userId) => {
    !onlineUsers.some((user) => user.userId == userId) &&
    onlineUsers.push({
        userId,
        sockedId:socket?.id
    })
    console.log("onlineuser",onlineUsers);
    socket.emit('getUsersOnline',onlineUsers)
  })
  socket.on('logout', () => {
    onlineUsers =onlineUsers.filter((data) => {
      console.log("socket.id != data?.sockedId");
       return socket.id != data?.sockedId
    })
    console.log("onlineuser",onlineUsers);
    socket.emit('getUsersOnline',onlineUsers)
  })
});

io.listen(3000, (e) => console.log("running socket",e));