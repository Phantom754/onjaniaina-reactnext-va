import { FC, PropsWithChildren, createContext, useContext, useEffect, useState } from "react"
import { useQueryListChat } from "../hooks"
import { ChatModel } from "../Model"
import { useSelector } from "react-redux"
import { RootState } from "@/app/appStore"
import { io } from "socket.io-client"

type Type = {
    allData: ChatModel[],
    refetch: (id:any) => void
    refetchUser: (id:any) => void
    isLoading:boolean,
    users:any[],
    onLine:any[]
    socket:any,
    logout: () => void
}

export const ChatContext = createContext<Type>({allData:[],refetch: () => {},isLoading:false,refetchUser:() => {},users:[],onLine:[], socket:null,logout: () =>{}})
export const ChatContextProvider: FC<PropsWithChildren> = ({children}) => {
    const {allData,isLoading,getChatDiscussions,getUser,users,} = useQueryListChat()
    const  [socket,setSoket] = useState<any>(null)
    const [onLine,setOnline] = useState<any[]>([])
    const user = useSelector((state: RootState) => state.profileUser)?.user;
    
    useEffect(() => {
        if(!user) return 
        const newSocket = io('http://localhost:3000')
        setSoket(newSocket)
        return () => {
          newSocket.disconnect()
        }
     }, [user]);

     useEffect(() => {
        if(socket == null || !user ) return
        socket.emit("addNewUser",user?._id)
        socket.on("getUsersOnline",(data: any[]) => setOnline(data))
        return () => {
            socket.off("getUsersOnline")
          }
     }, [socket,user]);

     const logout = () => {
       socket?.emit("logout",user?._id)
       socket.on("getUsersOnline",(data: any[]) => setOnline(data))
     }
    console.log('socket',socket);
    
    return (
        <ChatContext.Provider value={{refetch: getChatDiscussions, allData: allData ?? [],isLoading,refetchUser:getUser,users:users,onLine,socket,logout}}>
            {children}
        </ChatContext.Provider>
    )
}

export const useChatListData = () => useContext(ChatContext)