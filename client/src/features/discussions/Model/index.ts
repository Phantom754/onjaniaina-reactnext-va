export type ChatModel = {
    _id:        string;
    members:    Member[];
    created_at: Date | any;
    updatedAt:  Date | any;
    image?:string
    isActive?:boolean
}

export type Member = {
    _id:        string;
    name:       string;
    email:      string;
    image?:string
    created_at: Date;
    updatedAt:  Date;
}
