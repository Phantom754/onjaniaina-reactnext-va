import { RootState } from "@/app/appStore";
import { useEffect, useMemo, useState } from "react";
import { useSelector } from "react-redux";
import { DiscussionsServices } from "../services/DiscussionService";
import { useNavigate } from "react-router-dom";
import { ChatModel, Member } from "../Model";

export const useQueryListChat = () => {
    const [allData, setAllData] = useState<ChatModel[]>([]);
    const [allUserData, setAllUserData] = useState<any[]>([]);
    const [isLoading, setIsloading] = useState<boolean>(false);
    const user = useSelector((state: RootState) => state.profileUser)?.user;
     useEffect(() => {
        user?._id &&  getChatDiscussions(user._id);
      }, [user]);


    
      useEffect(() => {
        getUser();
      }, []);
      const users =useMemo(() => {
        const userPreselection =  allUserData.filter((userData) => {
            if (user?._id == userData?._id) return false
           let member:Member[] = [];
           allData.forEach((chat) =>{
              member = [...chat.members]
           })
           let id: string[] = []
           member.forEach((memberdata)=>{
             id.push(memberdata._id)
           })
           if(id.includes(userData?._id)) return false
           return true
        })
        console.log('userPreselection',userPreselection);
        
        return userPreselection
     },[allData,allUserData])
     
      const getChatDiscussions = async (id: string) => {
        try {
          setIsloading(true);
          const res = await DiscussionsServices.getUserDiscussions(id);
          setAllData(res?.data);
        } catch (error) {
        } finally {
          setIsloading(false);
        }
      };

      const getUser = async () => {
        try {
          const res = await DiscussionsServices.getUser();
          console.log('list user',res);
          setAllUserData(res?.data?.user ?? [])
          
        } catch (error) {
        } finally {
        }
      };
      
      return  {allData,isLoading,getChatDiscussions,getUser,users,}
}

export const useDetailChat = () => {
    const navigate = useNavigate();
    const handleClick = (id: string) => {
        navigate(`discussion/${id}`);
      };
    return {handleClick}  
}