import { authReducer } from "@/features/auth/reducers/Auth.reducers";
import { drawerReducer } from "@/shared/reducers/drawer/drawer.reducer";


export const appReducer = {
    profileUser: authReducer,
    drawerState: drawerReducer,
   
}