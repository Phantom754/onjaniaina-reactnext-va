export class Color {
  static PRIMARY = "#DA054A";
  static SECONDARY = "#677788";
  static GRAY = "#D2C8C8";
  static ACCENT = "#F4F5F7";
  static BLACK = "#A39F9F";
  static GRAY_BLACK = "#797575";
  static BROWN = "#83032C";
}
