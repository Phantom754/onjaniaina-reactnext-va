import { FC } from "react";
import styles from "./style.module.scss";
import InputEmoji from 'react-input-emoji'
type InputType = {
  name: string;
  placeholder?: string;
  value?: any;
  isDisable?: boolean;
  onChange: (e: any) => void;
  onEnter?: (text:string | undefined) => void
};

export const MessageInput: FC<InputType> = ({
  name,
  placeholder,
  onChange,
  value,
  isDisable,
  onEnter
}) => {
  return (
    <div className={styles.message_input__container}>
      {/* <input
        id={name}
        onChange={(e) => onChange({ [name]: e?.target?.value })}
        name={name}
        disabled={isDisable}
        value={value}
        placeholder={placeholder}
        className={styles.message_input__input}
      /> */}
       <InputEmoji
          value={value}
          onChange={(value) => onChange({ [name]: value })}
          cleanOnEnter
          onEnter={(text) => onEnter && onEnter(text)}
          placeholder={placeholder}
          disableRecent={isDisable}
        />
    </div>
  );
};
