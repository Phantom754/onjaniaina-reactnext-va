import { Search } from "@/shared/ui/search/Search";
import {  useState } from "react";
import { CardUser } from "./components/cardUser/CardUser";
import { Divider } from "@/shared/ui/divider/Divider";
import { useSelector } from "react-redux";
import { RootState } from "@/app/appStore";
import { Loader } from "@/shared/ui/loader/Loader";
import { useDetailChat } from "@/features/discussions/hooks";
import { useChatListData } from "@/features/discussions/Providers/ChatProvider";


export const Discussions = () => {
  const [searchText, setSearchText] = useState<string>("");
  const {allData,isLoading}=useChatListData()
  const  {handleClick}= useDetailChat()
  const user = useSelector((state: RootState) => state.profileUser)?.user;
  const {onLine}=useChatListData()



  const handleChange = (e: any) => {
    setSearchText((Object.values(e) as any)[0]);
  };

  const handleSubmitSearch = (e: any) => {
    e.preventDefault();
    console.log(searchText);
  };

  
  console.log(onLine);
  
  return (
    <div>
      <Loader isLoading={isLoading} />
      <form>
        <Search name="search" placeholder="Recherche" onChange={handleChange} />
        <button type="submit" onClick={handleSubmitSearch}></button>
      </form>

      <div className="">
        {allData?.map((item, key: number) => (
          <div key={key}>
            <CardUser
              id={item?._id}
              name={
                item?.members?.find((member: any) => member?._id !== user?._id)
                  ?.name || ''
              }
              subtitle={
                item?.members?.find((member: any) => member?._id !== user?._id)
                  ?.email
              }
              image={item?.image}
              isActive={onLine.some((userOnLIne:any) => userOnLIne?.userId == item?.members?.find((member: any) => member?._id !== user?._id)?._id )}
              onClick={handleClick}
            />
            {allData?.length > 1 && allData?.length - 1 !== key ? (
              <Divider />
            ) : (
              ""
            )}
          </div>
        ))}
      </div>
    </div>
  );
};


